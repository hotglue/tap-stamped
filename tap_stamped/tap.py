"""Stamped tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_stamped.streams import (
    ReviewsStream,
    StampedStream,
    RewardsStream,
    CustomersStream,
    CustomerRewardsStream
)

STREAM_TYPES = [ReviewsStream, RewardsStream, CustomersStream, CustomerRewardsStream]


class TapStamped(Tap):
    """Stamped tap class."""

    name = "tap-stamped"

    config_jsonschema = th.PropertiesList(
        th.Property("public_key", th.StringType, required=True),
        th.Property("private_key", th.StringType, required=True),
        th.Property("store_hash", th.StringType),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapStamped.cli()
