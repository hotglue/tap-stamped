"""REST client handling, including StampedStream base class."""

import logging
from typing import Any, Dict, Optional

import requests
from memoization import cached
from singer_sdk.authenticators import BasicAuthenticator
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream

logging.getLogger("backoff").setLevel(logging.CRITICAL)


class StampedStream(RESTStream):
    """Stamped stream class."""

    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        hash = self.config["store_hash"]
        return f"https://stamped.io/api/v2/{hash}"

    records_jsonpath = "$[*]"
    actual_page = "$.page"
    total_page_token_jsonpath = "$.totalPages"

    @property
    def authenticator(self) -> BasicAuthenticator:
        """Return a new authenticator object."""
        return BasicAuthenticator.create_for_stream(
            self,
            username=self.config.get("public_key"),
            password=self.config.get("private_key"),
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        content = response.json()
        try:
            actual_page = list(extract_jsonpath(self.actual_page, content))[0]
            total_pages = list(extract_jsonpath(self.total_page_token_jsonpath, content))[0]
        except IndexError:
            return None
        
        if actual_page is None or actual_page >= total_pages:
            return None
        return actual_page + 1

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["page"] = next_page_token
        if self.replication_key:
            params["sort"] = "asc"
            start_date = self.get_starting_timestamp(context)
            if start_date is not None:
                date = start_date.strftime("%Y-%m-%dT%H:%M:%S")
                params["dateFrom"] = date
        return params


    def _request(
        self, prepared_request: requests.PreparedRequest, context: dict 
    ) -> requests.Response:
        """TODO.

        Args:
            prepared_request: TODO
            context: Stream partition or context dictionary.

        Returns:
            TODO
        """
        response = self.requests_session.send(prepared_request, timeout=self.timeout)
        self._write_request_duration_log(
            endpoint=self.path,
            response=response,
            context=context,
            extra_tags={"url": prepared_request.path_url}
            if self._LOG_REQUEST_METRIC_URLS
            else None,
        )
        self.validate_response(response)
        return response