"""Stream type classes for tap-stamped."""

import hmac
import hashlib
import requests
from datetime import datetime

from singer_sdk import typing as th

from tap_stamped.client import StampedStream


class ReviewsStream(StampedStream):
    name = "reviews"
    path = "/dashboard/reviews"
    records_jsonpath = "$.results[*].review"
    primary_keys = ["id"]
    replication_key = "dateAdded"

    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("reviewState", th.NumberType),
        th.Property("rating", th.NumberType),
        th.Property("title", th.StringType),
        th.Property("author", th.StringType),
        th.Property("email", th.StringType),
        th.Property("location", th.StringType),
        th.Property("countryISO", th.StringType),
        th.Property("imagesFileName", th.StringType),
        th.Property("imagesFileNamePublished", th.StringType),
        th.Property("videosFileName", th.StringType),
        th.Property("videosFileNamePublished", th.StringType),
        th.Property("body", th.StringType),
        th.Property("reply", th.StringType),
        th.Property("productId", th.NumberType),
        th.Property("productHandle", th.StringType),
        th.Property("productTitle", th.StringType),
        th.Property("productUrl", th.StringType),
        th.Property("productImageUrl", th.StringType),
        th.Property("productImageCroppedUrl", th.StringType),
        th.Property("productImageThumbnailUrl", th.StringType),
        th.Property("sentimentScore", th.NumberType),
        th.Property("sentimentMagnitude", th.NumberType),
        th.Property("source", th.NumberType),
        th.Property("reviewSource", th.StringType),
        th.Property("ipAddress", th.StringType),
        th.Property("ipAddressSource", th.StringType),
        th.Property("ipAddressSource", th.StringType),
        th.Property("isPublishedShopify", th.BooleanType),
        th.Property("isConvertedImage", th.StringType),
        th.Property("isPublicReply", th.BooleanType),
        th.Property("isFeatured", th.BooleanType),
        th.Property("isRead", th.BooleanType),
        th.Property("isPublishedFeed", th.BooleanType),
        th.Property("isRecommend", th.BooleanType),
        th.Property("queueReviewEmailID", th.IntegerType),
        th.Property("personId", th.NumberType),
        th.Property("customerId", th.NumberType),
        th.Property("shopProductId", th.NumberType),
        th.Property("reviewSourceAppId", th.NumberType),
        th.Property("reviewSourceAppEntityId", th.NumberType),
        th.Property("shop_ID", th.NumberType),
        th.Property("verifiedType", th.NumberType),
        th.Property("dateCreated", th.DateTimeType),
        th.Property("dateReplied", th.DateTimeType),
        th.Property("dateAdded", th.DateTimeType),
        th.Property("dateUpdated", th.DateTimeType),
        th.Property("medialist", th.CustomType({"type": ["array", "string"]})),
        th.Property("optionsList", th.CustomType({"type": ["array", "string"]})),
        th.Property("votesList", th.CustomType({"type": ["array", "string"]})),
        th.Property("socialSharesList", th.CustomType({"type": ["array", "string"]})),
        th.Property("tagsList", th.CustomType({"type": ["array", "string"]})),
        th.Property(
            "sentimentAnalysisReviewList", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property(
            "customer",
            th.ObjectType(
                th.Property("id", th.NumberType),
                th.Property("name", th.StringType),
                th.Property("firstName", th.StringType),
                th.Property("lastName", th.StringType),
                th.Property("email", th.StringType),
                th.Property("customerEmail", th.StringType),
                th.Property("externalId", th.StringType),
                th.Property("uuid", th.StringType),
                th.Property("profileImageUrl", th.StringType),
                th.Property("notes", th.StringType),
                th.Property("tags", th.CustomType({"type": ["array", "string"]})),
                th.Property("country", th.StringType),
                th.Property("ipAddress", th.StringType),
                th.Property("ipAddressSource", th.StringType),
                th.Property("isSubscribed", th.BooleanType),
                th.Property("sentimentScore", th.NumberType),
                th.Property("sentiment", th.StringType),
                th.Property("points", th.NumberType),
                th.Property("totalSpent", th.NumberType),
                th.Property("isConfirmed", th.BooleanType),
                th.Property("vipTierId", th.NumberType),
                th.Property("dateCreated", th.DateTimeType),
                th.Property("dateUpdated", th.DateTimeType),
                th.Property("dateTierUpdated", th.DateTimeType),
                th.Property("dateTierEligibilityCheck", th.DateTimeType),
                th.Property("dateTierEligibilityUntil", th.DateTimeType),
                th.Property("dateLastLogin", th.DateTimeType),
                th.Property("dateLastActivityRewards", th.DateTimeType),
                th.Property("dateBirthday", th.DateTimeType),
                th.Property("shopId", th.NumberType),
                th.Property("vipTier", th.NumberType),
                th.Property("referralCode", th.StringType),
                th.Property("referralUrl", th.StringType),
            ),
        ),
        th.Property("orderId", th.StringType),
        th.Property("variant", th.StringType),
        th.Property("voteUp", th.NumberType),
        th.Property("voteDown", th.NumberType),
        th.Property("countSocialShare", th.NumberType),
        th.Property("tags", th.CustomType({"type": ["array", "string"]})),
        th.Property("topics", th.CustomType({"type": ["array", "string"]})),
        th.Property("isEdit", th.BooleanType),
    ).to_dict()


class RewardsStream(StampedStream):
    name = "rewards"
    records_jsonpath = "$.campaigns[*]"
    rest_method = "POST"
    path = ""
    schema = th.PropertiesList(
        th.Property("earnings", th.ArrayType(
            th.ObjectType(
                th.Property("id", th.NumberType),
                th.Property("dateCreated", th.DateTimeType),
                th.Property("campaignEvent", th.NumberType),
                th.Property("campaignEventString", th.StringType),
                th.Property("title", th.StringType),
                th.Property("titlePublic", th.StringType),
                th.Property("description", th.StringType),
                th.Property("urlImage", th.StringType),
                th.Property("isSystem", th.BooleanType),
                th.Property("isActive", th.BooleanType),
                th.Property("isEnabledRules", th.BooleanType),
                th.Property("isEnabledNotifications", th.BooleanType),
                th.Property("isCompleted", th.BooleanType),
                th.Property("points", th.NumberType),
                th.Property("pointsFullName", th.StringType),
        ))),
        th.Property("spendings", th.ArrayType(
            th.ObjectType(
                th.Property("codePrefix", th.StringType),
                th.Property("discountValue", th.NumberType),
                th.Property("entitledProductIds", th.StringType),
                th.Property("campaignType", th.NumberType),
                th.Property("campaignTypeString", th.StringType),
                th.Property("isRedeemable", th.BooleanType),
                th.Property("id", th.NumberType),
                th.Property("title", th.StringType),
                th.Property("titlePublic", th.StringType),
                th.Property("description", th.StringType),
                th.Property("urlImage", th.StringType),
                th.Property("isSystem", th.BooleanType),
                th.Property("isActive", th.BooleanType),
                th.Property("isEnabledLimits", th.BooleanType),
                th.Property("isEnabledNotifications", th.BooleanType),
                th.Property("dateCreated", th.DateTimeType),
            )
        )),
        th.Property("coupons", th.CustomType({"type": ["array", "string"]})),
        th.Property("referrals", th.CustomType({"type": ["object","array", "string"]})),
        th.Property("vip_tiers", th.CustomType({"type": ["array", "string"]})),        
    ).to_dict()

    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        hash = self.config["store_hash"]
        api_key = self.config["public_key"]
        return f"https://stamped.io/api/v2/rewards/init?sId={hash}&apiKey={api_key}"
    

class CustomersStream(StampedStream):
    name = "customers"
    records_jsonpath = "$.results[*]"
    path = "/dashboard/customers"
    replication_key = "dateUpdated"
    primary_keys = ["id"]
    schema = th.PropertiesList(
        th.Property("dateUpdated", th.DateTimeType),
        th.Property("id", th.NumberType),
        th.Property("customer", th.ObjectType(
            th.Property("name", th.StringType),
            th.Property("firstName", th.StringType),
            th.Property("lastName", th.StringType),
            th.Property("email", th.StringType),
            th.Property("customerEmail", th.StringType),
            th.Property("externalId", th.StringType),
            th.Property("uuid", th.StringType),
            th.Property("profileImageUrl", th.StringType),
            th.Property("notes", th.StringType),
            th.Property("tags", th.CustomType({"type": ["array", "string"]})),
            th.Property("country", th.StringType),
            th.Property("ipAddress", th.StringType),
            th.Property("ipAddressSource", th.StringType),
            th.Property("isSubscribed", th.BooleanType),
            th.Property("sentimentScore", th.NumberType),
            th.Property("sentiment", th.StringType),
            th.Property("points", th.NumberType),
            th.Property("totalSpent", th.NumberType),
            th.Property("isConfirmed", th.BooleanType),
            th.Property("vipTierId", th.NumberType),
            th.Property("dateCreated", th.DateTimeType),
            th.Property("dateUpdated", th.DateTimeType),
            th.Property("dateTierUpdated", th.DateTimeType),
            th.Property("dateTierEligibilityCheck", th.DateTimeType),
            th.Property("dateTierEligibilityUntil", th.DateTimeType),
            th.Property("dateLastLogin", th.DateTimeType),
            th.Property("dateBirthday", th.DateTimeType),
            th.Property("shopId", th.NumberType),
            th.Property("vipTier", th.NumberType),
            th.Property("referralCode", th.StringType),
            th.Property("referralUrl", th.StringType),
            th.Property("attentiveLastSyncDate", th.DateTimeType),
        )),
        th.Property("countReviews", th.NumberType),
        th.Property("countReferrals", th.NumberType),
        th.Property("points", th.NumberType),
        th.Property("redeemableReward", th.ArrayType(
            th.ObjectType(
                th.Property("campaignId", th.NumberType),
                th.Property("rewardTitle", th.StringType),
                th.Property("pointsRequired", th.NumberType),
            )
        )),
    ).to_dict()

    def get_url_params(self, context, next_page_token):
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["page"] = next_page_token
        if self.replication_key:
            start_date = self.get_starting_timestamp(context)
            if start_date is not None:
                date = start_date.strftime("%Y-%m-%dT%H:%M:%S")
                params["dateUpdatedMin"] = date
        return params

    def to_datetime(self, date):
        if date is None:
            return None
        try:
            return datetime.strptime(date, '%Y-%m-%dT%H:%M:%S.%fZ')
        except ValueError:
            return datetime.strptime(date, '%Y-%m-%dT%H:%M:%SZ')
        except TypeError:
            return None


    def post_process(self, row, context):
        row["dateUpdated"] = self.to_datetime(row["customer"]["dateUpdated"])
        row["id"] = row["customer"]["id"]
        return row

    def get_child_context(self, record, context):
        """Return a context dictionary for child streams."""
        customer_id_and_email = f'{record["customer"]["id"]}{record["customer"]["customerEmail"]}'
        return {
            "customer_email": record["customer"]["customerEmail"],
            "customer_id": record["customer"]["id"],
            "customer_auth_token": hmac.new(
                self.config["private_key"].encode("utf-8"),
                customer_id_and_email.encode('utf-8'),
                hashlib.sha256
            ).hexdigest(),
        }


class CustomerRewardsStream(StampedStream):
    name = "customer_rewards"
    path = ""
    parent_stream_type = CustomersStream
    ignore_parent_replication_keys = True
    records_jsonpath = "$"
    rest_method = "POST"

    @property
    def url_base(self) -> str:
        hash = self.config["store_hash"]
        return f"https://stamped.io/api/v2/rewards/init?sId={hash}&apiKey={self.config['public_key']}"
    
    def prepare_request_payload(self, context, next_page_token):
        """Prepare request payload."""
        return {
            "customerId": context["customer_id"],
            "customerEmail": context["customer_email"],
            "authToken": context["customer_auth_token"],
        }

    schema = th.PropertiesList(
        th.Property(
            "customer", th.ObjectType(
                th.Property("customerId", th.StringType),
                th.Property("customerEmail", th.StringType),
                th.Property("customerFirstName", th.StringType),
                th.Property("customerLastName", th.StringType),
                th.Property("customerTags", th.StringType),
                th.Property("customerLocale", th.StringType),
                th.Property("vipTierTitle", th.StringType),
                th.Property("vipTierId", th.NumberType),
                th.Property("vipTierGoalValue", th.NumberType),
                th.Property("vipTierEligibleValue", th.NumberType),
                th.Property("urlReferral", th.StringType),
                th.Property("totalOrders", th.NumberType),
                th.Property("totalSpent", th.NumberType),
                th.Property("isAcceptMarketing", th.BooleanType),
                th.Property("dateBirthday", th.IntegerType),
                th.Property("dateVipTierEntered", th.DateTimeType),
                th.Property("dateVipTierUntil", th.DateTimeType),
                th.Property("authToken", th.StringType),
            )),
            th.Property("points", th.ObjectType(
                th.Property("points", th.NumberType),
                th.Property("points_current_with_name", th.StringType),
            )),
            th.Property("earnings", th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.NumberType),
                    th.Property("dateCreated", th.DateTimeType),
                    th.Property("campaignEvent", th.NumberType),
                    th.Property("campaignEventString", th.StringType),
                    th.Property("title", th.StringType),
                    th.Property("titlePublic", th.StringType),
                    th.Property("description", th.StringType),
                    th.Property("urlImage", th.StringType),
                    th.Property("isSystem", th.BooleanType),
                    th.Property("isActive", th.BooleanType),
                    th.Property("isEnabledRules", th.BooleanType),
                    th.Property("isEnabledNotifications", th.BooleanType),
                    th.Property("isCompleted", th.BooleanType),
                    th.Property("totalCount", th.NumberType),
                    th.Property("pointsFullName", th.StringType),
                    th.Property("points", th.NumberType),
            ))),
            th.Property("spendings", th.ArrayType(
                th.ObjectType(
                    th.Property("codePrefix", th.StringType),
                    th.Property("discountValue", th.NumberType),
                    th.Property("entitledProductIds", th.StringType),
                    th.Property("entitledCollectionIds", th.StringType),
                    th.Property("campaignType", th.NumberType),
                    th.Property("isRedeemable", th.BooleanType),
                    th.Property("totalCount", th.NumberType),
                    th.Property("campaignTypeString", th.StringType),
                    th.Property("pointsFullName", th.StringType),
                    th.Property("id", th.NumberType),
                    th.Property("title", th.StringType),
                    th.Property("titlePublic", th.StringType),
                    th.Property("description", th.StringType),
                    th.Property("urlImage", th.StringType),
                    th.Property("points", th.NumberType),
                    th.Property("isSystem", th.BooleanType),
                    th.Property("isActive", th.BooleanType),
                    th.Property("isEnabledNotifications", th.BooleanType),
                    th.Property("isEnabledLimits", th.BooleanType),
                    th.Property("dateCreated", th.DateTimeType),
                )
            )),
            th.Property("coupons", th.CustomType({"type": ["array", "string"]})),
            th.Property("referrals", th.CustomType({"type": ["object","array", "string"]})),
            th.Property("vip_tiers", th.CustomType({"type": ["array", "string"]})),
        ).to_dict()